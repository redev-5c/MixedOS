<img src="1591238168168.png">

# 1\.0.2 Update Now available to the world!
MixedOS Developer redev is now announcing the MixedOS V 1.2!
You should have it because...
<ul>
  <li>Alexa for Non-NFC Edition</li>
  <li>More natural translation</li>
  <li>Source code on GitHub!</li>
  <li>More device support</li>
  <li>Minor Bugfix</li>
  <li>Custom notification</li>
</ul>

# Wanna contribute?

Just mention me on <strong>Issues</strong> tab.
I will go there within 48 hours and reply to your proposal.


# GitHub Source code license
This program is using MIT License.
Therefore, you should read (and understand) terms of use there.

# Copyrighted Materials (Should not be used as commercial use!)
I have no rights about programs, dlls, jsons listed below.
<ul>
<li>Watchface.exe</li>
<li>mb4_resource_tool.exe</li>
<li>mb4_lang_tool.exe</li>
</ul>
<strong>If you are owner of these files and have all rights, please add issue and I will immidately delete it.</strong>