No molestar
Alarma
Cámara
Música
Cronómetro
Temporizador
Encontrar disp
Silenciar
Tarjetas
Alipay
Pantalla
Ajustes
Brillo
Bloqueo de pantalla
Reiniciar
Reseteo de fábrica
Verif info
Acerca de
Ritmo de brazada
Estado
Ritmo cardíaco
Entrenamiento
Tiempo
Mi Home
Notificaciones
Más
Vincular\nprimero
¿Vincular el\nteléfono?
Vinculado\ncon éxito
Cargado\ncompleta-\nmente
Cargar\nahora
Batería por\ndebajo del\n%s%%, cárgala\nahora
Actualizando...
No se pudo actualizar
Actualizado con éxito
Actualizando...
Actualizado con éxito
No se pudo actualizar
Sincro…
Sincronizado con éxito
No se pudo sincronizar
Actualizando...
Actualizado con éxito
No se pudo actualizar
Conectar con\nMi Fit para\nvolver a\nactualizar
Pasos
Distancia
Calorías
Estar senta-\ndo durante\ndemasiado\ntiempo no es\nbueno. In-\ncrementa el\nriesgo de\npadecer va-\nrias enfer-\nmedades co-\nmo diabetes\ny espondilo-\nsis cervical\nPara rela-\njarte pue-\ndes levan-\ntarte y mo-\nverte du-\nrante un\nminuto.
km
Abrocha la \npulsera li-\ngeramente\npor encima\ndel hueso de\ntu muñeca\n(1 cm más o\nmenos) y man\ntente quieto
Vuelve a intentarlo
Resetea la\ncontraseña\nen los ajus-\ntes de blo-\nqueo de pan-\ntalla o rea-\nliza un re-\nseteo de fá-\nbrica en Mi\nFit
Zona
Relajado
Ligero
Intenso
Aeróbico
Anaeróbico
VO₂ máx
Es un impor-\ntante indi-\ncador de la\nsalud car-\ndiopulmonar\nde una per-\nsona. Una\npersona tie-\nne un RCR\n(ritmo car-\ndíaco en re-\nposo) medio\nde alrededor\n60-80 LPM.
Correr al aire libre
Cinta de correr
Ciclismo
Caminar
Libre
Natación en piscina
Espacio de\nalmacena-\nmiento a\npunto de\nllenarse,\nsincroniza\ntus datos
Sin espacio\nsuficiente,\nlos datos no\nsincroniza-\ndos podrían\nperderse si\ncontinúas
Las rutas no\nserán registra-\ndas si tu telé-\nfono está des-\nconectado
Abre Mi Fit\ny activa el\nGPS en tu\nteléfono
Localizando...\nLa señal es \nmejor en\náreas abiertas
Localizado con éxito
Ajusta la pul-\nsera para ob-\ntener datos\nmás precisos
La pulsera\nregistrará\ntu ruta des-\npués de ob\ntener infor-\nmación de\nlocalización
La distancia\nes demasia\ndo corta pa-\nra ser re-\ngistrada
El tiempo es\ndemasiado\ncorto para\nser regis-\ntrado
Cuando el entrenamiento comienza
Los datos son\nmás bajos que\nen la sesión \ndel teléfono
Cuando el teléfono se desconecta
Volver a conectar con el teléfono
Señal débil
Tamaño de piscina
Personalizar
Tiempo
Ritmo
Cadencia
Velocidad
Pausado
¿Finalizar\nla sesión?
GPS dispo-\nnible
Señal GPS\ndébil
GPS no\ndisponible
Teléfono\ndesconectado
Teléfono co-\nnectado
Sesión fina-\nlizada debi-\ndo a espacio\nde almacena-\nmiento insu-\nficiente
La tempera-\ntura actual\nes más alta\nen tu área,\nprograma la\nhora de\nejercicio a-\npropiadamen-\nte para evi-\ntar golpes\nde calor
La tempera-\ntura actual\nes más baja\nen tu área,\nmantente ca-\nliente y no\ncamines por\ncaminos con\nhielo
Ritmo cardíaco demasiado alto
Ritmo demasiado lento
Último km
Última milla
Zancada
Aumento de elevación
Pérdida de elevación
Los datos\nserán cali-\nbrados en la\npágina de\ndetalles de\nentrenamien-\nto de Mi Fit
Velocidad media
Velocidad máxima
Distancia escalada
Brazadas
Índice SWOLF
Vueltas totales
BPM medio
BPM máximo
DPB media
LPM máximo
LPM medio
m
cm
km/h

Calidad del aire
índice del aire
Excelente
Bueno
Hoy
Mañana
LUN
MAR
MIÉ
JUE
VIE
SÁB
DOM
Conecta con Mi Fit para actualizar la información del tiempo
Añadir ciudades en Mi Fit
Alertas de tiempo
Crea tareas en la página de ajustes de la pulsera de la aplicación Mi Home
Instrucción enviada
Borrar todo
Borrado
Sin notificaciones
Has estado sentado demasiado tiempo
te ha \nenviado\nun zumbido
Desconectado
Sobre rojo
Completado\nobjetivo de %s\npasos
AM
PM
Recordatorio de evento
No molestar desactivado
No molestar inteligente activado
No molestar activado
Activar
Activación automática
Desactivar No molestar
Activar auto- máticamente No molestar después de dormirte
Todos los días
Establecer alarmas en Mi Fit
Vibrará en\n10 minutos
Alarma\ndesactivada
Abrir cámara
Teléfono desconectado
Para usar\nesta función\nprimero vin-\ncula el\nBluetooth en\nMi Fit
No se pudo conectar con el teléfono
Un momento...
No se está reproduciendo música
No se puede obtener el nombre de la canción
¿Finalizar  ahora?
El tiempo ha terminado
Tonos activados
Desactivar alarma
Un momento...
Silencio activado
Silenciar
Activar alarma
Un momento...
Alarma activada
Un momento...
Alarma sonando
Acerca la pulsera al lector
Un momento...
Un momento...
Añadir tarjetas en Mi Fit
Completa el pago en %s segundos
No se pudo usar esta tarjeta, pulsa la pantalla para volver a intentarlo
El sistema está\nocupado, vuel-\nve a intentarlo\nmás tarde
La tarjeta está bloqueada, contacta con nuestro servicio de atención al cliente
Balance (CNY)
Fondos insuficientes
El saldo es de menos de 10 CNY, recárgalo ahora
Escanea el código para pagar
Siguiente
Después de\nañadir Ali-\npay puedes\npagar mos-\ntrando el\ncódigo de\npago en la\npulsera
Abre Alipay\nen tu telé-\nfono para\ncomprobar el\nacuerdo de\nservicio en\nla página de\nla pulsera\ninteligente
Aceptar el acuerdo
Escanea el código con Alipay para añadir
Un momento...
Añadido con éxito
No se pudo añadir
Eliminar
Después de\neliminar\nAlipay no\npodrás pa-\ngar con tu\npulsera
Activado
Desactivado
Enciende la\npantalla y\ndesliza ha-\ncia arriba\npara desblo-\nquear
¿Reiniciar ahora?
Después \nde restablecer\na los valores\nde fábrica,\nvincúlala de\nnuevo con Mi\nFit.
Normativa
¿Puedo ayudarte?
Un momento…
Perdona, ¿podrías repetirlo?
Lo siento, no puedo hacer eso, pero puedo ayudarte a controlar la pulsera y dispositivos de Mi Home
Conexión agotada. ¿Volver a intentarlo?
El teléfono no está conectado a la red
Para usar el Altavoz IA Mi conéctalo con tu teléfono
Permitir al Altavoz IA Mi controlar dispositivos en Mi Fit
No se puede añadir, puedes añadir hasta 50 recordatorios de eventos
No se puede añadir una alarma para más de 24 horas, se añadirá un recordatorio de evento
No se puede añadir, puedes añadir hasta 10 alarmas
No se puede establecer un temporizador para más de 99 minutos y 59 segundos
No se pudo  completar la acción, vuelve a intentarlo
Mantén pulsado para pausar
No se pudo obtener información
Llamada entrante
No se pudo medir
El Altavoz AI Mi está solo disponible en China continental, si tienes cualquier pregunta contacta con el servicio de atención al cliente.
Finalizado
Alertas de inactividad
Pace
Pace
Carga la pulsera
No se puede usar el Altavoz IA Mi mientras la pulsera sincroniza datos
Lun a Vie
Añadir evento
Versión de firmware
Dirección Bluetooth
Estilo principal
Estilo libre
Braza
Espalda
Mariposa
Mixto
Alerta
Un momento...
Apagar pantalla automáticamente
Por defecto
Mantener la\npantalla activa\ndurante mucho\ntiempo reduce\nla vida de la\nbatería
Bajo
Medio
Alto
min
h
min
PAI (Personal\nActivity Intel-\nligence)\nes un índice\nde ejercicio\npersonal que\nevalúa el\nefecto del\nejercicio ba-\nsándose en\nel género,\nedad, y datos\nde ritmo\ncardíaco. Estu-\ndios del pro-\nyecto HUNT\nFitness Study\nmuestran que\nlas personas\nque mantienen\nsu PAI por\nencima de 100\ntienen un me-\nnor riesgo de\npadecer pre-\nsión sanguinea\nalta, enferme-\ndades del co-\nrazón y diabe-\ntes tipo 2.
¡Buen trabajo!
Siguiente objetivo
¡Bien hecho!
¡Enhorabuena!
Sigue así
El servicio\nde tarjetas\nde transporte\nno está dis-\nponible en\ntu región
El servicio\nde tarjetas\nbancarias\nno está dis-\nponible en\ntu región
¿Vincular la pulsera?
Cal
Ejercicio
Para usar la\nfunción PAI\nestablece el\nmétodo de de-\ntección como\ndetección au-\ntomática de\nritmo cardíaco\ny la frecuencia\ncomo una vez\npor minuto.
