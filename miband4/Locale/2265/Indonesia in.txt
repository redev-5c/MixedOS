Jgn ganggu
Alarm
Kamera
Musik
Stopwatch
Timer
Cari perangkat
Hening
Kartu
Alipay
Tampilan
Setelan
Kecerahan
Layar kunci
Mulai ulang
Reset ke awal
Info verifikasi
Tentang
Nilai stroke
Status
Detak jantung
Latihan
Cuaca
Mi Home
Notifikasi
Lainnya
Pasang dulu
Pasangkan\ntelepon?
Terpasang
Terisi penuh
Isi daya
Baterai di\nbawah  %s%%,\nisi daya\nsekarang
Memperbarui...
Tidak dapat\nmemperbarui
Berhasil\nmemperbarui
Memperbarui...
Berhasil\nmemperbarui
Tidak dapat memperbarui
Sinkronisasi...
Berhasil menyinkron
Tidak dapat menyinkron
Memperbarui...
Berhasil\nmemperbarui
Tidak dapat memperbarui
Hubungkan ke\nMi Fit untuk \nperbarui lagi
Langkah
Jarak
Kalori
Duduk \nterlalu lama\nberbahaya\nbagi kesehatan.\nHal tersebut\nakan\nmeningkatkan\nrisiko berbagai\npenyakit \nseperti\ndiabetes &\nmasalah leher\natau punggung.
km
Kencangkan\nband sedikit\nsekitar 1cm\ndi atas\npergelangan\ntangan &\njangan gerak
Coba lagi
Reset sandi\ndi setelan\nkunci band\natau setel\nulang pabrik\ndi Mi Fit
Zona
Santai
Ringan
Intensif
Aerobik
Anaerob
VO₂ max
Indikator\npenting bagi\nkesehatan\nkardiopul-\nmoner\nseseorang.\nRata-rata\nRHR (detak\njantung\nistirahat):\n60-80 BPM.
Lari outdoor
Treadmill
Bersepeda
Jalan
Bebas
Berenang di kolam
Ruang\npenyimpanan\nhampir penuh,\nsinkronkan\ndata Anda
Tidak ada\ncukup ruang,\ndata yang\nbelum\ntersinkron\nmungkin hilang\njika dilanjutkan
Rute tidak\nakan dicatat\njika telepon \nAnda terputus
Buka Mi Fit,\nhidupkan GPS\ntelepon Anda
Memperoleh\ninfo lokasi...\nSinyal lebih \nbaik di area\nterbuka
Berhasil\nmemperoleh\ninfo lokasi
Kencangkan\nband untuk\nmendapatkan\ndata yang\nlebih akurat
Band akan\nmencatat rute\nsetelah\nmemperoleh\ninfo lokasi
Jarak terlalu\npendek untuk\ndicatat
Waktu terlalu\npendek untuk\ndicatat
Saat latihan dimulai
Penyinkronan \ndata pada band\n sedikit lebih \nlambat \ndibanding \nsesi pada \ntelepon
Saat telepon terputus
Hubungkan ke ponsel lagi
Sinyal lemah
Ukuran kolam
Kustom
Waktu
Tempo
Irama
Kecepatan
Dijeda
Akhiri sesi?
GPS tersedia
Sinyal GPS\nlemah
GPS tidak\ntersedia
Telepon\nterputus
Telepon\nterhubung
Sesi berakhir\nkarena ruang\npenyimpanan\ntidak cukup
Suhu daerah\nsaat ini tinggi,\natur waktu \nolahraga dgn\nbaik untuk\nmencegah\nsengatan\npanas
Suhu daerah\nsaat ini rendah\nJaga agar\ntetap hangat\ndan hati-hati\nberjalan di\njalan yang\nlicin.
Detak jantung terlalu tinggi
Tempo terlalu lambat
Km terakhir
Mi terakhir
Langkah
Peningkatan ketinggian
Kehilangan ketinggian
Data dapat\ndikalibrasikan\npada halaman\nrincian latihan\nMi Fit
Kecepatan rata2
Kecepatan maks
Jarak terpanjat
Stroke
Indeks SWOLF
Total putaran
SPM rata2
SPM maks
DPS rata2
BPM maks
BPM rata2
m
cm
km/j

Kualitas udara
Indeks udara
Sangat bagus
Bagus
Hari ini
Besok
SEN
SEL
RAB
KAM
JUM
SAB
MIN
Hubungkan ke\nMi Fit untuk\nmemperbarui\ninfo cuaca
Tambah kota di Mi Fit
Peringatan cuaca
Buat tugas pada halaman setelan band apl Mi Home
Instruksi terkirim
Hapus semua
Terhapus
Tidak ada\nnotifikasi
Anda sudah\nduduk terlalu\nlama
menyolek Anda
Terputus
Amplop merah
Target %s langkah tercapai
AM
PM
Pengingat acara
Jangan Ganggu\ndimatikan
Jangan Ganggu\nPintar\ndihidupkan
Jangan Ganggu\ndihidupkan
Hidupkan
Otomatis hidupkan
Matikan Jangan Ganggu
Otomatis \nhidupkan\nJangan Ganggu\nsetelah Anda\ntertidur
Setiap hari
Setel alarm\npada Mi Fit
Ingatkan saya\nsetelah 10 mnt
Mati
Buka kamera
Telepon terputus
Untuk\nmenggunakan\nfitur ini,\npasangkan\nBluetooth di\nMi Fit dulu
Tidak dapat \nterhubung ke \ntelepon
Tunggu sebentar...
Tidak ada musik yang diputar
Tidak dapat peroleh nama lagu
Akhiri sekarang?
Waktu telah habis
Alarm hidup
Matikan alarm
Tunggu sebentar...
Alarm mati
Diam
Hidupkan alarm
Tunggu sebentar...
Alarm hidup
Tunggu sebentar...
Alarm berbunyi
Dekatkan band ke pembaca
Tunggu sebentar...
Tunggu sebentar...
Tambah kartu \ndi Mi Fit
Selesaikan \npembayaran \ndalam %s dtk
Tidak dapat menggunakan kartu ini, ketuk tampilan untuk coba lagi
Sistem sibuk, \ncoba lagi nanti
Kartu ini terblokir, hubungi layanan pelanggan kami
Saldo (CNY)
Dana tidak cukup
Saldo kurang dari 10 CNY, isi ulang sekarang
Pindai kode untuk bayar
Berikutnya
Setelah\nmenambahkan\nAlipay, Anda\ndpt membayar\ndengan\nmenunjukkan\nkode\npembayaran\npada band
Buka Alipay\ndi telepon\nutk memeriksa\nperjanjian\nlayanan di\nhalaman band\npintar
Menyetujui perjanjian
Pindai kode dengan Alipay untuk menambahkan
Tunggu sebentar...
Berhasil ditambahkan
Tidak dapat menambahkan
Hapus
Setelah Alipay\nterhapus,\nAnda tidak\nakan dapat\nmembayar\nmenggunakan\nband
Hidup
Mati
Nyalakan layar\ndan geser ke\natas untuk\nbuka kunci
Mulai ulang sekarang?
Setelah setel\nulang pabrik,\npasangkan\nlagi ke Mi Fit\nagar dapat \nmemakai band
Peraturan
Ada yang bisa\ndibantu?
Tunggu\nsebentar
Bisa Anda \nulangi lagi?
Maaf, saya \ntidak bisa \nmelakukan ini, \ntetapi saya \ndpt membantu\nmengontrol \nband dan\nperangkat \nMi Home
Waktu koneksi\nhabis. Coba\nlagi?
Telepon tidak \nterhubung ke\njaringan
Untuk\nmenggunakan \nMi AI Speaker,\nhubungkan ke\ntelepon Anda
Izinkan Mi AI\nSpeaker kon-\ntrol perangkat\ndi Mi Fit
Tidak dapat \nmenambahkan.\nMaksimal 50\npengingat\nacara.
Tidak dapat \nmenambahkan\nalarm untuk \nlebih dari 24\njam. Akan \ndibuat menjadi \npengingat \nacara.
Tidak dapat\nmenambahkan.\nMaksimal 10 \nalarm.
Tidak dapat \nmenyetel timer\nselama lebih \ndari 99 menit \ndan 59 detik
Tidak dapat \nmenyelesaikan\naksi, coba lagi
Tekan lama untuk jeda
Tidak dapat \nperoleh info
Panggilan masuk
Tidak dapat mengukur
Mi AI Speaker\nhanya tersedia\ndi daratan\nTiongkok saat \nini, jika Anda \nmemiliki \npertanyaan,\nkontak layanan\npelanggan.
Berakhir
Peringatan diam
Pace
Pace
Isi daya band
Tidak dapat \nmenggunakan\nMi AI saat\nband sedang\nmenyinkron \ndata
Sen - Jum
Tambah acara
Versi firmware
Alamat Bluetooth
Gaya utama
Gaya bebas
Gaya dada
Gaya punggung
Gaya kupu-kupu
Gaya campuran
Pengingat
Tunggu sebentar...
Layar auto mati
Standar
Layar yang\nhidup dalam\nwaktu yang\nlama akan\nmengurangi\ndaya baterai
Rendah
Sedang
Tinggi
mnt
j
m
PAI (Personal\nActivity Intel-\nligence) adalah\nindeks latihan\npribadi yang\nmengevaluasi\nefek latihan\nberdasarkan\njenis kelamin,\nusia, dan data\ndetak jantung.\nStudi oleh\nproyek HUNT\nFitness Study\nmenunjukkan\nbahwa orang\nyang mem-\npertahankan\nnilai PAI di\natas 100\nmemiliki risiko\ntekanan darah\ntinggi,penyakit\njantung, dan\ndiabetes tipe 2\nyang lebih\nrendah.
Hebat!
Target berikut
Bagus!
Selamat!
Ayo semangat!
Kartu trans-\nportasi tidak\ntersedia di\ndaerah Anda
Kartu bank\ntidak tersedia\ndi daerah\nAnda
Pasangkan band?
Kal
Olahraga
Untuk meng-\ngunakan fitur\nPAI, setel\nmetode\ndeteksi sebagai\notomatis\ndeteksi detak\njantung dan\nsetel frekuensi\nmenjadi 1x per\nmenit.
